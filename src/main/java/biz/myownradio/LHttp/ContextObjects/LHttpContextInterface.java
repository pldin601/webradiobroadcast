package biz.myownradio.LHttp.ContextObjects;

/**
 * Created by Roman on 30.10.14.
 */
public interface LHttpContextInterface {

    public boolean is(String path);

    public String getContext();

}
