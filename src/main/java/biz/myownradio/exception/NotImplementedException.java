package biz.myownradio.exception;

/**
 * Created by Roman on 08.10.14.
 */
public class NotImplementedException extends RuntimeException {
    public NotImplementedException() {
        super();
    }
}
