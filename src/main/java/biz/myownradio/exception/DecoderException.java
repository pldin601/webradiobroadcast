package biz.myownradio.exception;

public class DecoderException extends RuntimeException {

    public DecoderException(String message) {
        super(message);
    }

    public DecoderException() {
        super();
    }

}
